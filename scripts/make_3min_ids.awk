BEGIN {
	FS="\t"
	OFS="\t"
}
{
	if (NR==1){$1=$1 "\t0_IID";print;next;}
	min = substr($1,19,2)
	min_id = int((min / 3))*3
	$1=$1 "\t" substr($1,1,18) sprintf("%02d",min_id);
	print $0
}