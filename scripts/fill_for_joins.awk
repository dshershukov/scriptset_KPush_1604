BEGIN{
	FS="\t"
	OFS="\t"
	char="\t"
	len=0
	#     1 2 3 4 5 6 7 8 9101112131415
	dum="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
}
{
	if (NR==1){len = NF;print $0;next;}
	if (NF < len){$0 = $0 "" substr(dum,1,(len-NF));}
	print $0;
}