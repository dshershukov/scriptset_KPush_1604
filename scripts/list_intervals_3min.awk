BEGIN {
	i = 0
	type = "0"
	
	FS = "\t"
	OFS = "\t"
	print "0_IID"
}

{
	i = i + 1
	if (FNR < i) {
		i = 1; type = "1";
	}
	if (type == 1) {
		apaper[i] = $0
	} else {
		gsub("\.","");
		adate[i] = substr($0,5,4) "" substr($0,3,2) "" substr($0,1,2);
	}
}

END {
	for (p in apaper) {
		for (d in adate) {
			for (i = 8; i <= 19; i++) {
				for (j = 0; j < 60; j = j + 3) {
					print apaper[p] "" adate[d] "" sprintf("%02d",i) "" sprintf("%02d",j)
				}
			}
		}
	}
} 
