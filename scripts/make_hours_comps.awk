BEGIN{
	FS="\t"
	OFS="\t"
}
{
	if (NR == 1) { print $1, "COMPS", $2; next; }
	$1 = $1 "\t" substr($1,1,8);
	print $0;
}