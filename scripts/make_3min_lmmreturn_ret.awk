BEGIN{
	FS="\t"
	OFS="\t"
}
{
	if (NR == 1) { print $0, "lmmreturn_ret"; next; }
	if ($4 == "") { $5 = ""; print $0; next; }
	$4 == 0 ? $5 = "" : $5 = ($3 - $4)/$4
	print $0;
}