BEGIN{
	FS="\t"
	OFS="\t"
	prev_val=""
	prev_id=""
	fid=2
	fsrc=6
}

{
	if (NR == 1) { print "0_IID","lmprice"; next; }
	if (NR == 2) { prev_val = $fsrc; prev_id = $fid; next; }
	if ($fid != prev_id) { print prev_id, prev_val; prev_id = $fid;}
	prev_val = $fsrc;
}

END{
	print prev_id, prev_val;
}