BEGIN{
	FS="\t"
	OFS="\t"
	onr=0
}
{
	if ($3 == "Timestamp") {
		if (NR == 1) {
			$1 = "0_File"
			$2 = "Row in file\tRow" 
			print $0
		}
		next
	}
	
	if ($5 == "" || $5 == 0 || $7 == "" || $7 == 0 || $9 == 0 || $9 == "") {next}
	
	onr++
	n_row = sprintf("%09d", onr)
	$2 = $2 "\t" n_row
	
	print $0
}
