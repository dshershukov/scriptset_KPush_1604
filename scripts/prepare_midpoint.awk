BEGIN {
	FS="\t"
	OFS="\t"
	b_ask = 12
	b_bid = 10
	m_point = 18
}

{
	if (NR == 1) {$m_point = "midpoint"; print $0; next;}
	midpoint_v = ($b_ask + $b_bid)/2
	print $0, midpoint_v
}
