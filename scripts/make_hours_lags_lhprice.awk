BEGIN {
	FS="\t"
	OFS="\t"
	fid = 2
	fsrc = 3
	fdest = 4
	prev_id = ""
	prev_val = 0
	
	new_name = "lhprice_lags"
}

{
	if (NR == 1) {$fdest = new_name; print $0; prev_id = $fsrc; next;}
	
	if ($fid != prev_id) {
		$fdest = ""
		prev_val = $fsrc
		prev_id = $fid
	} else {
		$fdest = prev_val
		prev_val = $fsrc
	}
	print $0
} 
