BEGIN {
	FS="\t"
	OFS="\t"
	fid = 2
	fsrc = 5
	fdest = 10
	prev_id = ""
	prev_val = ""
	
	new_name = "lag_trade_price"
}

{
	if (NR == 1) {$fdest = new_name; print $0; prev_id = $fsrc; next;}
	
	if ($fid != prev_id) {
		$fdest = ""
		prev_val = $fsrc
		prev_id = $fid
	} else {
		$fdest = prev_val
		prev_val = $fsrc
	}
	print $0
}