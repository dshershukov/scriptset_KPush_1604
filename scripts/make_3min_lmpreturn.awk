BEGIN{
	FS="\t"
	OFS="\t"
	prev_val=""
	prev_id=""
	fid=2
	fsrc=7
}

{
	if (NR == 1) { print "0_IID","lmpreturn"; next; }
	if (NR == 2) { prev_val = $fsrc; prev_id = $fid; next; }
	if ($fid != prev_id) { print prev_id, prev_val; prev_id = $fid;}
	prev_val = $fsrc;
}

END{
	print prev_id, prev_val;
}