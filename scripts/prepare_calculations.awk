function abs(v){ return (v < 0 ? -v : v) }

BEGIN {
	
	FS="\t"
	OFS="\t"
	
	#  1	LID
	#  2	PAPER
	#  3	NR for data
	#  4	Timestamp
	#  5	Trade Price
	#  6	Trade Volume
	#  7	Best Bid
	#  8	Best Ask
	#  9	MIDPOINT
	# 10	LAG Trade Price
	# 11	LAG MIDPOINT
	# 12	TRADEVOL
	# 13	TRADEVOL_Q
	# 14	ESPREAD_LOG
	# 15	ESPREAD
	# 16	QSPREAD_LOG
	# 17	QSPREAD
	# 18	PRETURN
	# 19	MRETURN
	# 20	SIGN_A
	# 21	SIGN_B
	# 22	SVOL_A
	# 23	SVOL_B
	
	flid = 1
	fpaper = 2
	fnr = 3
	ftimestamp = 4
	ftprice = 5
	ftvol = 6
	fbbid = 7
	fbask = 8
	fmpoint = 9
	ftprice_l = 10
	fmpoint_l = 11
	ftradevol = 12
	ftradevol_q = 13
	fespread_log = 14
	fespread = 15
	fqspread_log = 16
	fqspread = 17
	fpreturn = 18
	fmreturn = 19
	fsigna = 20
	fsignb = 21
	fsvola = 22
	fsvolb = 23
	
}

{
	if (NR == 1) {print $0, "tradevol", "tradevol_q", "espread_log", "espread", "qspread_log", "qspread", "preturn", "mreturn", "sign_a", "sign_b", "svol_a", "svol_b"; next;}
	
	# Всё, для чего не нужны лаги
	$ftradevol = $ftprice * $ftvol
	$ftradevol_q = $ftvol
	$fespread_log = 2 * abs(log($ftprice)-log($fmpoint))
	$fespread = 2 * abs(($ftprice - $fmpoint)/$fmpoint)
	$fqspread_log = log($fbask) - log($fbbid)
	$fqspread = ($fbask - $fbbid)/$fmpoint
	
	# Всё, для чего нужны лаги или что-то из этого зависит от лагов (кроме sign_a и svol_a, они не зависят, но оставлены для красоты)
	
	# preturn, mreturn
	$ftprice_l != "" ? $fpreturn = log($ftprice) - log($ftprice_l) : $fpreturn = ""
	$fmpoint_l != "" ? $fmreturn = log($fmpoint) - log($fmpoint_l) : $fmreturn = ""
	
	# sign_a
	$ftprice == $fmpoint ? $fsigna = 0 : ( (abs($fbask - $ftprice) < abs($fbbid - $ftprice)) ? $fsigna = 1 : $fsigna = -1 )
	
	# sign_b
	$ftprice == $fmpoint ? ( $ftprice_l == "" ? $fsignb = "" : ($ftprice == $ftprice_l ? $fsignb = 0 : ( $ftprice > $ftprice_l ? $fsignb = 1 : $fsignb = -1) ) ) : $fsignb = $fsigna
	
	# signed volumes
	$fsvola = sqrt($ftradevol) * $fsigna
	$fsignb == "" ? $fsvolb = "" : $fsvolb = sqrt($ftradevol) * $fsignb
	
	print $0
}