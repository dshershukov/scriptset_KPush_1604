BEGIN{
	FS="\t"
	OFS="\t"
	
	fid=2;
	fsrc=18;
	
	prev_id="";
	csum="";
	ccount=0;
	
	new_name="qspread";
	
}

{
	if (NR == 1) { print "0_HID", new_name, "q_" new_name; next; }
	if (NR == 2) {
		if ($fsrc == "") {
			csum=$fsrc; ccount=0; prev_id=$fid; next;
		} else {
			csum=$fsrc; ccount=1; prev_id=$fid; next;
		}
	}
	if (prev_id == $fid) {
		if ($fsrc != "") {
			csum = csum + $fsrc;
			ccount++;
		}
	} else {
		print prev_id, (csum/ccount), ccount;
		if ($fsrc == "") {
			csum=$fsrc; ccount=0; prev_id=$fid; next;
		} else {
			csum=$fsrc; ccount=1; prev_id=$fid; next;
		}
	}
}

END{
	print prev_id, (csum/ccount), ccount;
}