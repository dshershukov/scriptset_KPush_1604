BEGIN{
	FS="\t"
	OFS="\t"
	
	fid=2;
	fsrc=10;
	
	prev_id="";
	csum="";
}

{
	if (NR == 1) { print "0_IID", "svol_b"; next; }
	if (NR == 2) { csum=$fsrc; prev_id=$fid; next; }
	if (prev_id == $fid) { csum = csum + $fsrc; } else { print prev_id, csum; csum=$fsrc; prev_id = $fid; }
}

END{
	print prev_id, csum;
}