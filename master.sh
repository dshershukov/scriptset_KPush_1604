# 

echo $(date) "*** Preparing for calculations ***" | tee -a log
# На этапе подготовки к вычислениям таблица с данными приводится в единый формат, готовый для работы

echo $(date) "Renaming files to maintain compatibility with MAWK" | tee -a log
# Для сохранения качественной работы mawk с именами файлов необходимо провести их дополнительную обработку
# В обработку входит удаление всех точек, кроме точек перед расширением и удаление пробелов
cd ./raw_files; rename 's/ //g' *; rename 's/\.MM/MM/g' *; rename 's/\.L/L/g' *; rename 's/\.F/F/g' *; rename 's/\.O/O/g' *; cd ..

echo $(date) "Preparing register of file and paper pairs" | tee -a log
mawk 'BEGIN{FS="\t";OFS="\t";}{gsub(" ","",$1);gsub("\.MM","MM",$1);gsub("\.F","F",$1);gsub("\.L","L",$1);gsub("\.O","O",$1);print $0;}' ./settings/file_names_raw | sort -t $'\t' -k1,1 > ./work_files/file_names_sorted

echo $(date) "*** Preparing data ***" | tee -a log
# Из всех имеющихся колонок выбираются только те, которые потом понадобятся, и приводятся в нужный порядок
# Формат на выходе:
#  1	FILENAME
#  2	FNR
#  3	"Timestamp"
#  4	"Last"
#  5	"Trade Price"
#  6	"Trade Volume"
#  7	"Best Bid"
#  8	"Bid Size"
#  9	"Best Ask"
# 10	"Ask Size"
# 11	"Turnover"
# 12	"Calc VWAP"
# 13	"Flow"
# 14	"Trade Flags"

# После этого из них удаляются \r и nbsp

# Далее используется mawk-скрипт, в котором:
# Из всех имевшихся данных остаются только те строки, в которых есть данные по bid, ask и price
# Удаляются строки, где есть ненужные заголовки
# Составляется индекс по времени и номеру записи
# Формат на выходе:
#  1	FILENAME
#  2	FNR
#  3	NR for data
#  4	Timestamp
#  5	Last
#  6	Trade Price
#  7	Trade Volume
#  8	Best Bid
#  9	Bid Size
# 10	Best Ask
# 11	Ask Size
# 12	Turnover
# 13	Calc VWAP
# 14	Flow
# 15	Trade Flags

echo $(date) "Collecting files" | tee -a log
mawk -f ./scripts/get_required_cols.awk raw_files/* | sed 's/\x0D//g' | sed 's/\xA0//g' | mawk '{FS="\t";OFS="\t"; if (($4 == "" || $4 == 0 || $6 == "" || $6 == 0 || $8 == 0 || $8 == "")==0) {print $0}}' | mawk -f ./scripts/prepare_rows.awk | mawk -f ./scripts/prepare_number_format.awk | sort -t $'\t' -k1,1 -s -S70% > ./work_files/raw_data

# После сортировки объединяем с данными по ценным бумагам
# Затем вычисляем идентификаторы записей вида БУМАГА-ДАТА-ВРЕМЯ-NR (LID)
# На выходе получаем формат:
#  1	LID
#  2	FILENAME
#  3	PAPER
#  4	FNR
#  5	NR for data
#  6	Timestamp
#  7	Last
#  8	Trade Price
#  9	Trade Volume
# 10	Best Bid
# 11	Bid Size
# 12	Best Ask
# 13	Ask Size
# 14	Turnover
# 15	Calc VWAP
# 16	Flow
# 17	Trade Flags

# После этого сортируем по LID и получаем те же данные, но теперь точно в хронологическом порядке.
# После того, как сделали сортировку, начинаем вычисления. До разделения имеет смысл вычислять только midpoint.
# После этого для ускорения делаем чай в отдельный файл, а потом оставляем только нужные для вычислений колонки.

# На выходе получаем в файле, который уходит на вывод:
#  1	LID
#  2	FILENAME
#  3	PAPER
#  4	FNR
#  5	NR for data
#  6	Timestamp
#  7	Last
#  8	Trade Price
#  9	Trade Volume
# 10	Best Bid
# 11	Bid Size
# 12	Best Ask
# 13	Ask Size
# 14	Turnover
# 15	Calc VWAP
# 16	Flow
# 17	Trade Flags
# 18	MIDPOINT

# На выходе получаем в основном потоке (после добавления лагов):
#  1	LID
#  2	PAPER
#  3	NR for data
#  4	Timestamp
#  5	Trade Price
#  6	Trade Volume
#  7	Best Bid
#  8	Best Ask
#  9	MIDPOINT
# 10	LAG Trade Price
# 11	LAG MIDPOINT

# Затем производим вычисления, для которых не нужны агрегации

# На выходе получаем:
#  1	LID
#  2	PAPER
#  3	NR for data
#  4	Timestamp
#  5	Trade Price
#  6	Trade Volume
#  7	Best Bid
#  8	Best Ask
#  9	MIDPOINT
# 10	LAG Trade Price
# 11	LAG MIDPOINT
# 12	TRADEVOL
# 13	TRADEVOL_Q
# 14	ESPREAD_LOG
# 15	ESPREAD
# 16	QSPREAD_LOG
# 17	QSPREAD
# 18	PRETURN
# 19	MRETURN
# 20	SIGN_A
# 21	SIGN_B
# 22	SVOL_A
# 23	SVOL_B

echo $(date) "Joining and splitting files" | tee -a log
join -a2 -1 1 -2 1 -t $'\t' -o2.1,1.2,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,2.10,2.11,2.12,2.13,2.14,2.15 ./work_files/file_names_sorted ./work_files/raw_data | mawk -f ./scripts/prepare_LID.awk | tee >(mawk -f ./scripts/get_each_1.awk > ./waiting_for_merge/waiting_1) | tee >(mawk -f ./scripts/get_each_2.awk > ./waiting_for_merge/waiting_2) | tee >(mawk -f ./scripts/get_each_3.awk > ./waiting_for_merge/waiting_3) | tee >(mawk -f ./scripts/get_each_4.awk > ./waiting_for_merge/waiting_4) | mawk -f ./scripts/get_each_5.awk > ./waiting_for_merge/waiting_5

echo $(date) "Sorting the splitted files" | tee -a log
cat ./waiting_for_merge/waiting_1 | sort -t $'\t' -k1,1 -S70% > ./waiting_for_merge/ready_1
cat ./waiting_for_merge/waiting_2 | sort -t $'\t' -k1,1 -S70% > ./waiting_for_merge/ready_2
cat ./waiting_for_merge/waiting_3 | sort -t $'\t' -k1,1 -S70% > ./waiting_for_merge/ready_3
cat ./waiting_for_merge/waiting_4 | sort -t $'\t' -k1,1 -S70% > ./waiting_for_merge/ready_4
cat ./waiting_for_merge/waiting_5 | sort -t $'\t' -k1,1 -S70% > ./waiting_for_merge/ready_5

echo $(date) "Merging and running per-line calculations" | tee -a log
# sort -t $'\t' -k1,1 -S15% -m ./waiting_for_merge/ready_1 ./waiting_for_merge/ready_2 | sort -t $'\t' -k1,1 -S15% -m ./waiting_for_merge/ready_3 | sort -t $'\t' -k1,1 -S15% -m ./waiting_for_merge/ready_4 | sort -t $'\t' -k1,1 -S15% -m ./waiting_for_merge/ready_5 | tee test | mawk -f ./scripts/prepare_midpoint.awk | tee ./result/raw_data | cut -f1,3,5,6,8,9,10,12,18 | mawk -f ./scripts/prepare_lags_trade_price.awk | mawk -f ./scripts/prepare_lags_midpoint.awk | mawk -f ./scripts/prepare_calculations.awk > ./work_files/prepared_data

sort -t $'\t' -k1,1 -S70% -m ./waiting_for_merge/ready_* | tee test | mawk -f ./scripts/prepare_midpoint.awk | tee ./result/raw_data | cut -f1,3,5,6,8,9,10,12,18 | mawk -f ./scripts/prepare_lags_trade_price.awk | mawk -f ./scripts/prepare_lags_midpoint.awk | mawk -f ./scripts/prepare_calculations.awk > ./work_files/prepared_data

# Для дальнейшей работы необходимо получить из ./work_files/prepared_data информацию о промежутках времени в один час и в три минуты.
# Для формирования этих списков сначала делаем списки уникальных бумаг и датах для этих бумаг.
# Для трёхминутных таблиц указана минута начала, идентификатор трёхминутный можно получить, разделив минуты на три, взяв целую часть и умножив её на три.

echo $(date) "Preparing tables" | tee -a log
cat ./work_files/prepared_data | tee >(mawk 'BEGIN{FS="\t";OFS="\t"}{if (NR > 1) {print substr($4,1,10)}}' | sort | uniq > ./work_files/list_dates) | mawk 'BEGIN{FS="\t";OFS="\t"}{if (NR > 1) { print $2 }}' | sort | uniq  >./work_files/list_papers
mawk -f ./scripts/list_hours.awk ./work_files/list_* | sort > ./work_files/raw_hours_table
mawk -f ./scripts/list_intervals_3min.awk ./work_files/list_* | sort > ./work_files/raw_3min_table

# Начинаем обработку данных по часам
# Для начала оставляем только нужные для работы столбцы
# Затем для каждого обработчика при помощи tee дублируем поток, производим вычисления и пишем результат в файл

echo $(date) "*** Running final calculations ***" | tee -a log
echo $(date) "Calculating data for hours" | tee -a log
cat ./work_files/prepared_data | cut -f1-19 | mawk -f ./scripts/make_hours_ids.awk | tee ./work_files/prepared_data_hours | tee >(mawk -f ./scripts/make_hours_lhprice.awk | mawk -f ./scripts/make_hours_comps.awk | mawk -f ./scripts/make_hours_lags_lhprice.awk | mawk -f ./scripts/make_hours_lhprice_ret.awk | cut -f1,3,4,5 > ./work_files/res_hours_lhprice ) | tee >(mawk -f ./scripts/make_hours_sum_tradevol.awk > ./work_files/res_hours_tradevol ) | tee >(mawk -f ./scripts/make_hours_sum_tradevol_q.awk > ./work_files/res_hours_tradevol_q) | tee >(mawk -f./scripts/make_hours_av_espread_log.awk > ./work_files/res_hours_espread_log) | tee >(mawk -f ./scripts/make_hours_av_espread.awk > ./work_files/res_hours_espread) | tee >(mawk -f ./scripts/make_hours_av_qspread_log.awk > ./work_files/res_hours_qspread_log) | tee >(mawk -f ./scripts/make_hours_av_qspread.awk > ./work_files/res_hours_qspread) | tee >(mawk -f ./scripts/make_hours_ssq_preturn.awk > ./work_files/res_hours_ssq_preturn) | mawk -f ./scripts/make_hours_ssq_mreturn.awk > ./work_files/res_hours_ssq_mreturn

# Затем собираем все файлы в один с помощью join
# Чтобы было легко добавлять или убирать новые вычисления, формат колонок в командах join не указывается.
# Вместо этого недостающие разделители в конце добавляются с помощью скрипта fill_for_joins.awk, поддерживающего добавление до 15 колонок.

echo $(date) "Collecting data for hours into a table" | tee -a log
join -t $'\t' -a1 -1 1 -2 1 ./work_files/raw_hours_table ./work_files/res_hours_lhprice | mawk -f ./scripts/fill_for_joins.awk | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/res_hours_tradevol | mawk -f ./scripts/fill_for_joins.awk | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/res_hours_tradevol_q | mawk -f ./scripts/fill_for_joins.awk | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/res_hours_espread_log | mawk -f ./scripts/fill_for_joins.awk | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/res_hours_espread | mawk -f ./scripts/fill_for_joins.awk | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/res_hours_qspread_log | mawk -f ./scripts/fill_for_joins.awk | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/res_hours_qspread | mawk -f ./scripts/fill_for_joins.awk | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/res_hours_ssq_preturn | mawk -f ./scripts/fill_for_joins.awk | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/res_hours_ssq_mreturn | mawk -f ./scripts/fill_for_joins.awk | mawk -f ./scripts/make_hours_trim.awk > ./result/hours_table

# После того, как завершилась обработка по часам, делаем обработку по минутам
# После прохождения cut и подготовки идентификаторов интервалов данные имеют вид:

#  1	LID
#  2	IID
#  3	PAPER
#  4	NR for data
#  5	Timestamp
#  6	Trade Price
#  7	PRETURN
#  8	MRETURN
#  9	SVOL_A
# 10	SVOL_B

echo $(date) "Calculating data for 3-minute intervals" | tee -a log
cat ./work_files/prepared_data | cut -f1-5,18-19,22-23 | mawk -f ./scripts/make_3min_ids.awk | tee ./work_files/prepared_data_3min | tee >(mawk -f ./scripts/make_3min_lmpreturn.awk | mawk -f ./scripts/make_3min_comps.awk | mawk -f ./scripts/make_3min_lags_lmpreturn.awk | mawk -f  ./scripts/make_3min_lmpreturn_ret.awk | cut -f1,3,4,5 > ./work_files/res_3min_lmpreturn ) | tee >(mawk -f ./scripts/make_3min_lmmreturn.awk | mawk -f ./scripts/make_3min_comps.awk | mawk -f ./scripts/make_3min_lags_lmmreturn.awk | mawk -f  ./scripts/make_3min_lmmreturn_ret.awk | cut -f1,3,4,5 > ./work_files/res_3min_lmmreturn ) | tee >(mawk -f ./scripts/make_3min_lmprice.awk | mawk -f ./scripts/make_3min_comps.awk | mawk -f ./scripts/make_3min_lags_lmprice.awk | mawk -f  ./scripts/make_3min_lmprice_ret.awk | cut -f1,3,4,5 > ./work_files/res_3min_lmprice) | tee >(mawk -f ./scripts/make_3min_sum_svola.awk > ./work_files/res_3min_svola) | mawk -f ./scripts/make_3min_sum_svolb.awk > ./work_files/res_3min_svolb

echo $(date) "Collecting data for 3-minute intervals into a table" | tee -a log
join -t $'\t' -a1 -1 1 -2 1 ./work_files/raw_3min_table ./work_files/res_3min_lmpreturn | mawk -f ./scripts/fill_for_joins.awk | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/res_3min_lmmreturn | mawk -f ./scripts/fill_for_joins.awk | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/res_3min_lmprice | mawk -f ./scripts/fill_for_joins.awk | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/res_3min_svola | mawk -f ./scripts/fill_for_joins.awk | join -t $'\t' -a1 -1 1 -2 1 - ./work_files/res_3min_svolb | mawk -f ./scripts/fill_for_joins.awk | mawk -f ./scripts/make_3min_trim.awk > ./result/3min_table

echo $(date) "DONE" | tee -a log